## Synopsis

Here is a GUI displaying data (user details, his posts and related comments) by
retrieving them from an API.

## Motivation

I created this simple app from scratch, from the wireframes on paper to coding.
It's a powerful way for having a full perspective of what designing web
application means. The user interface is willingly simple and poor. The only
requirement was to make the design fully responsive for fit with as many
screen devices as possible.

## Techno used

Ajax
Languages : HTML, CSS, Less, JavaScript (ECMAScript 5)
frameworks/libraries : AngularJS 1.6.4, AngularJS Material Design 1.1.4
local development/testing : http-server 0.10.0

## User interface - design

After creating wireframes and user journey to define the possible actions of the
user, i defined the definitive layouts and guidelines for for mobile, tablet and
desktop/laptop devices. Here are the targeted screen sizes:
from 320px width to 1920px width (and more).

## installation

After dowloading the app folder here, simply launch it on a local server like
http-server installed via npm (https://www.npmjs.com/package/http-server).
At the root of the project, lauch the server with the command: http-server

## API reference

https://jsonplaceholder.typicode.com


