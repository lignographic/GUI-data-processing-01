angular.module('postList')
.component('postList', {
    templateUrl : 'shared/components/post/post-list/post-list.template.html',
    controller : ['$http', 'dataUserService', 'dataPostService', function PostListController($http, dataUserService, dataPostService){
        var self = this;

        var dataFromUser = dataUserService.getAllDataUser();
        dataFromUser.then(function(data){
            self.dataUser = data;
        });

        var postsFromUser = dataPostService.getAllDataPost();
        postsFromUser.then(function(data){
            self.post = data;
        });
    }
   ]
});