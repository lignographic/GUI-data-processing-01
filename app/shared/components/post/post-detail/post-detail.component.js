angular.module('postDetail')
.component('postDetail', {
    templateUrl : 'shared/components/post/post-detail/post-detail.template.html',
    controller : ['$http', '$routeParams', 'dataUserService', 'dataPostService', function PostDetailController($http, $routeParams, dataUserService, dataPostService){
        var self = this;
        self.postId = $routeParams.postId;

        var postsFromUser = dataUserService.getAllDataUser();
        postsFromUser.then(function(data){
            self.user = data;
        });

        var dataFromUser = dataPostService.getAllDataPost();
        dataFromUser.then(function(data){
            self.postDetail = data;
        });

        $http.get('https://jsonplaceholder.typicode.com/comments?postId=' + self.postId)
        .then(function(response){
            self.comments = response.data;
        });

    }
    ]
});