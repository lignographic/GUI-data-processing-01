angular.module('userDetail')
.component('userDetail', {
    templateUrl : 'shared/components/user-detail/user-detail.template.html',
    controller : ['$http', 'dataUserService', function userListController($http, dataUserService){
        var self = this;
        var address = [],
        geoAddress = [];
        self.address = address;
        self.geoAddress = geoAddress;

        var callDataUser = dataUserService.getAllDataUser();
        callDataUser.then(function(data){
            self.dataUser = data;
            var tabObj = [];
            var tabArray = [];
            var sub = [];

            (function extractObjectFromUserData(){
                angular.forEach(self.dataUser, function(item){
                    if (typeof(item) === 'object'){
                        tabObj.push(item);
                    }
                })

                angular.forEach(tabObj[0], function(item){
                    if(typeof(item) === 'object'){
                        for(var i = 0 in item){
                            geoAddress.push(item[i]);
                        }
                    } else {
                        address.push(item);
                    }
                })
            })();
        });
        }
    ]
});