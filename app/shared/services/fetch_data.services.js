var root = 'https://jsonplaceholder.typicode.com';
var userId = 5;

appDataUser.factory('dataUserService', ['$http', '$q', function($http, $q){
    return {
        getAllDataUser : function(){
            var myPromise = $q.defer();
            $http.get(root + '/users/' + userId)
            .then(function(response){
                myPromise.resolve(response.data);
            }, myPromise.reject);
            return myPromise.promise;
        }
    };
}]);


appDataUser.service('dataPostService', ['$http', '$q', function($http, $q) {
    return {
        getAllDataPost : function(){
            var myPromise = $q.defer();
            $http.get(root + '/posts?userId=' + userId)
            .then(function(response){
                myPromise.resolve(response.data);
            }, myPromise.reject);
            return myPromise.promise;
        }
    };
}]);


appDataUser.service('dataComment', ['$http', '$q', function($http, $q){
    return{
        getAllDataComment : function(){
            var myPromise = $q.defer();
            $http.get(root + '/comments?postId=1')
            .then(function(response){
                myPromise.resolve(response.data);
            }, myPromise.reject);
            return myPromise.promise;
        }
    };
}]);
