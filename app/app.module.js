var appDataUser = angular.module("appDataUser", ['ngMaterial', 'ngRoute','myHeader', 'myAside', 'userDetail', 'postList', 'postDetail', 'myFooter', 'ngAnimate']);

appDataUser.config(function($locationProvider, $routeProvider){

    $locationProvider.hashPrefix('');

    $routeProvider
        .when('/user_detail',
        {
            title : 'user detail',
            template : '<user-detail></user-detail>'
        })
        .when('/post_list',
        {
            title : 'post list',
            template : '<post-list></post-list>'
        })
        .when('/post_list/:postId',
        {
            title : 'post detail',
            template : '<post-detail></post-detail>'
        }).
        otherwise('/user_detail');
});

appDataUser.run(['$location', '$rootScope', function($location, $rootScope){
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous){
        $rootScope.title = current.$$route.title;
    });
}]);